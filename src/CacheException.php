<?php
/**
 * PHP FIG
 *
 * @category     PSR-6
 * @package      Cache
 * @license      MIT. See also the LICENCE
 */
namespace Tiat\Psr\Cache;

/**
 * Exception interface for all exceptions thrown by an Implementing Library.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface CacheException extends Psr\Cache\CacheException {

}
